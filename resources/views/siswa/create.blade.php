@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Siswa</strong>
                            <small>Form</small>
                        </div>
                        <div class="card-body">
                            <form id="form-siswa" action="{{ route('siswa.store') }}" class="form-horizontal" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="nis">NIS</label>
                                    <input class="form-control" id="nis" name="nis" type="text" placeholder="NIS Siswa">
                                </div>
                                <div class="form-group">
                                    <label for="nama_siswa">Nama Siswa</label>
                                    <input class="form-control" id="nama_siswa" name="nama_siswa" type="text" placeholder="Nama siswa">
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <input class="form-control" id="alamat_siswa" name="alamat_siswa" type="text" placeholder="Alamat siswa">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" type="submit">
                                <i class="fa fa-dot-circle-o"></i> Submit</button>
                                <button class="btn btn-sm btn-danger" type="reset">
                                <i class="fa fa-ban"></i> Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
