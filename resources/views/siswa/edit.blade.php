@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <strong>Siswa</strong>
                            <small>Form</small>
                        </div>
                        <div class="card-body">
                            <form id="form-siswa" action="{{ route('siswa.update',$siswa->id) }}" class="form-horizontal" method="POST">
                                @csrf
                                
                                <input type="hidden" name="_method" value="PUT">

                                <div class="form-group">
                                    <label for="nis">NIS</label>
                                    <input class="form-control" id="nis" name="nis" type="text" placeholder="NIS Siswa" value="{{ $siswa->nis }}">
                                </div>
                                <div class="form-group">
                                    <label for="nama_siswa">Nama Siswa</label>
                                    <input class="form-control" id="nama_siswa" name="nama_siswa" type="text" placeholder="Nama siswa" value="{{ $siswa->nama_siswa }}">
                                </div>
                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <input class="form-control" id="alamat_siswa" name="alamat_siswa" type="text" placeholder="Alamat siswa" value="{{ $siswa->alamat_siswa }}">
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-sm btn-primary" type="submit">
                                Submit</button>
                                <button class="btn btn-sm btn-danger" type="reset">
                                Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
