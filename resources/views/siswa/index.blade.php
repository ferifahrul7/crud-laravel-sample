@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <a class="btn btn-sm btn-primary float-right" href="{{ route('siswa.create') }}">
                                Tambah
                            </a>
                        </div>
                        <div class="float-right">
                            <div class="controls">
                                <form action="{{ $data['search'] }}" id="search">
                                    <div class="input-group">
                                        <input class="form-control form-control-sm" name="term" id="term" type="text" placeholder="">
                                        <span class="input-group-append">
                                        <a class="btn btn-sm btn-secondary" href="{{ $data['search'] }}" onclick="event.preventDefault();
                                                     document.getElementById('search').submit();">
                                   CARI
                                </a>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-responsive-sm table-striped">
                            <thead>
                                <tr>
                                    <th>NIS</th>
                                    <th>Nama Siswa</th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($siswa as $data)
                                <tr>
                                    <td>{{ $data->nis }}</td>
                                    <td>{{ $data->nama_siswa }} </td>
                                    <td>{{ $data->alamat_siswa }}</td>
                                    <td width="150">
                                        <form action="{{ route('siswa.destroy',$data->id) }}" class="form-horizontal" method="POST">
                                            <div class="input-group">
                                                <a class="btn btn-sm btn-primary" href="{{ route('siswa.edit',$data->id) }}">
                                                    EDIT
                                                </a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    DELETE
                                            </div>
                                        </form>
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection